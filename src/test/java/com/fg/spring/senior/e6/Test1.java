package com.fg.spring.senior.e6;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import junit.framework.Assert;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes= {Config.class})
@ActiveProfiles("pro")
public class Test1 {
	@Autowired
	private TestBean testBean;

	@Test
	public void m1() {
		String pro = "pro";
		Assert.assertEquals(pro, testBean.getContent());
	}
	

}
