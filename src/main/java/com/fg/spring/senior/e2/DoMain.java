package com.fg.spring.senior.e2;

import java.util.Arrays;
import java.util.List;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class DoMain {
	public static void main(String[] args) {
		AnnotationConfigApplicationContext acac = new AnnotationConfigApplicationContext(Config.class);
		Service1 s1 = acac.getBean(Service1.class);
		List<String> list = Arrays.asList("1", "2", "3", "4", "5", "6", "7", "8", "9", "10");
		list.forEach(obj -> {
			s1.m1(obj);
			s1.m2(obj);
		});
		acac.close();
	}
}
