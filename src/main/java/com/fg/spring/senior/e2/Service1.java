package com.fg.spring.senior.e2;

import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

@Service
@Async
public class Service1 {
	int i = 0;
	
	public synchronized void m1(String n) {
		i++;
		System.out.println("第"+i+"个任务进入");
	}
	
	public void m2(String n) {
		System.out.println("任务编号为"+n);
	}

}
