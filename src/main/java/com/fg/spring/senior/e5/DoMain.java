package com.fg.spring.senior.e5;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import com.fg.spring.senior.e5.Config;

public class DoMain {
	public static void main(String[] args) {
		AnnotationConfigApplicationContext acac = new AnnotationConfigApplicationContext(Config.class);
		Service1 s1 = acac.getBean(Service1.class);
		s1.m1();
		acac.close();
		
	}
}
