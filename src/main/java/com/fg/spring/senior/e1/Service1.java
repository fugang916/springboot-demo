package com.fg.spring.senior.e1;

import java.io.IOException;

import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.BeanNameAware;
import org.springframework.context.ResourceLoaderAware;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.stereotype.Service;

@Service
public class Service1 implements BeanNameAware,ResourceLoaderAware{
	private String beanName;
	private ResourceLoader resourceLoader;
	
	@Override
	public void setResourceLoader(ResourceLoader resourceLoader) {
		this.resourceLoader = resourceLoader;
	}
	
	@Override
	public void setBeanName(String beanName) {
		this.beanName = beanName;
	}
	
	public void outputResource() throws IOException {
		System.out.println("bean的名称为"+beanName);
		Resource r = resourceLoader.getResource("classpath:com/fg/spring/senior/e1/test.txt");
		System.out.println("读取内容是"+IOUtils.toString(r.getInputStream(),"UTF-8"));
	}
}
