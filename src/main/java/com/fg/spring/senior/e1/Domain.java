package com.fg.spring.senior.e1;

import java.io.IOException;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class Domain {
	public static void main(String[] args) throws IOException {
		AnnotationConfigApplicationContext acac = new AnnotationConfigApplicationContext(Config.class);
		Service1 s = acac.getBean(Service1.class);
		s.outputResource();
		acac.close();
	}
}
