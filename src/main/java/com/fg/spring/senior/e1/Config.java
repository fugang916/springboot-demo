package com.fg.spring.senior.e1;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan("com.fg.spring.senior.e1")
public class Config {

}
