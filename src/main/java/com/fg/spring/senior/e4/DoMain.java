package com.fg.spring.senior.e4;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import com.fg.spring.senior.e4.Config;

public class DoMain {
	public static void main(String[] args) {
		AnnotationConfigApplicationContext acac = new AnnotationConfigApplicationContext(Config.class);  
		Service1 se = acac.getBean(Service1.class);
		System.out.println(se.getPath());
		acac.close();
	}
}
