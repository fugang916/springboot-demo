package com.fg.spring.senior.e4;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Conditional;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan("com.fg.spring.senior.e4")
public class Config {
	@Bean
	@Conditional(LinuxCondiction.class)
	public Service1 getImpl1() {
		return new Impl1();
	}
	
	@Bean
	@Conditional(WindowsCondiction.class)
	public Service1 getImpl2() {
		return new Impl2();
	}
}
