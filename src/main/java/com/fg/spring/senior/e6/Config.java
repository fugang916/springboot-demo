package com.fg.spring.senior.e6;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

@Configuration
public class Config {
	@Bean
	@Profile("dev")
	public TestBean devTestBean() {
		return new TestBean("dev");
	}
	
	
	@Bean
	@Profile("pro")
	public TestBean proTestBean() {
		return new TestBean("pro");
	}
}
