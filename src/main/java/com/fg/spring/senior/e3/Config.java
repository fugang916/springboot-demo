package com.fg.spring.senior.e3;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;

@Configuration
@ComponentScan("com.fg.spring.senior.e3")
@EnableScheduling
public class Config {

}
