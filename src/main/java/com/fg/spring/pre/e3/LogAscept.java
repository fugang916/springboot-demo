package com.fg.spring.pre.e3;


import java.lang.reflect.Method;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.stereotype.Component;

@Aspect
@Component
public class LogAscept {

	@Pointcut("@annotation(com.fg.spring.pre.e3.Action)")
	public void m3() {
		System.out.println("m3 run.");
	}
	
	@After("m3()")
	private void after(JoinPoint jo) {
		MethodSignature ms = (MethodSignature) jo.getSignature();
		Method m = ms.getMethod();
		Action act = m.getAnnotation(Action.class);
		System.out.println("【after】方法"+m.getName()+" 执行 切面 .");
		
	}
	
	@Before("execution(* com.fg.spring.pre.e3.*.*(..))")
	private void before(JoinPoint jo) {
		MethodSignature ms = (MethodSignature) jo.getSignature();
		Method m = ms.getMethod();
		System.out.println("【before】方法"+m.getName()+" 执行 切面 .");
	}
	
	@Around("execution(* com.fg.spring.pre.e3.*.*(..))")
	private Object around(ProceedingJoinPoint joinPoint) throws Throwable {
		Object[] arg = joinPoint.getArgs();
		if(arg!=null && arg.length!=0) {
			arg[0] = arg[0].toString().replace("呵呵", "哈哈");
			System.out.println("【before】替换后为"+joinPoint.getArgs()[0]);
		}
		Object rvt = joinPoint.proceed(arg);
		return rvt;
	}
	
	
}
