package com.fg.spring.pre.e3;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;

@Configuration
@ComponentScan("com.fg.spring.pre.e3")
@EnableAspectJAutoProxy
public class Config {

}
