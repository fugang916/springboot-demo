package com.fg.spring.pre.e3;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;


public class DoMain {
	private static AnnotationConfigApplicationContext acac;
	public static void main(String[] args) {
		acac = new AnnotationConfigApplicationContext(Config.class);
		Service1 s1 = acac.getBean(Service1.class);
		Service2 s2 = acac.getBean(Service2.class);
		System.out.println(s1.m1("�Ǻ�"));
		s2.m2();
		acac.close();
	}
}
