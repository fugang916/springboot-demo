package com.fg.spring.pre.e3;

import org.springframework.stereotype.Service;

@Service
public class Service1 {
	
	@Action(name="切点")
	public String m1(String res) {
		System.out.println("m1 run.");
		return "处理后的" + res;
	}
}
