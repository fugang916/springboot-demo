package com.fg.spring.pre.e1;

import org.springframework.stereotype.Service;

@Service
public class Service1 {
	public void m1() {
		System.out.println("m1 run.");
	}
}
