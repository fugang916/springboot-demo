package com.fg.spring.pre.e1;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class DoMain {
	private static AnnotationConfigApplicationContext acac;

	public static void main(String[] args) {
		acac = new AnnotationConfigApplicationContext(Config.class);
		Service2 service2 = acac.getBean(Service2.class);
		service2.m2();
		acac.close();
		
	}
}
