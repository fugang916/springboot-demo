package com.fg.spring.pre.e1;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan("com.fg.spring.pre.e1")
public class Config {

}
