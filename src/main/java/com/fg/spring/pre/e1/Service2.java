package com.fg.spring.pre.e1;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class Service2 {
	@Autowired
	Service1 service1;
	
	public void m2() {
		service1.m1();
		System.out.println("m2 run.");
	}
}
