package com.fg.spring.pre.e2;

public class Service2 {
	Service1 service1;


	public void setService1(Service1 service1) {
		this.service1 = service1;
	}
	
	public void m2() {
		service1.m1();
		System.out.println("m2 run.");
	}
	
}
