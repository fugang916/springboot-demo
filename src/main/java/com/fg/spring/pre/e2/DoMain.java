package com.fg.spring.pre.e2;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;


public class DoMain {

	public static void main(String[] args) {
		AnnotationConfigApplicationContext acac = new AnnotationConfigApplicationContext(Config.class);
		Service2 service2 = acac.getBean(Service2.class);
		service2.m2();
		acac.close();
	}
}
