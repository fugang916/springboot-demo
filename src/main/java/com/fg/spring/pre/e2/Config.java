package com.fg.spring.pre.e2;

import org.springframework.context.annotation.Bean;

public class Config {
	@Bean
	public Service1 buildService1() {
		return new Service1();
	}
	@Bean
	public Service2 buildService2() {
		Service2 service2 = new Service2();
		service2.setService1(buildService1());
		return service2;
	}
}
