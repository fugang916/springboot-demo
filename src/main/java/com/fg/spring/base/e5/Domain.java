package com.fg.spring.base.e5;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class Domain {
	public static void main(String[] args) {
		AnnotationConfigApplicationContext acac = new AnnotationConfigApplicationContext(Config.class); 
		Pusher1 p = acac.getBean(Pusher1.class);
		p.publish("ev1");
		acac.close();
	}
}
