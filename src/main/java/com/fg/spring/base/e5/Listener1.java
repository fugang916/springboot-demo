package com.fg.spring.base.e5;

import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;

@Component
public class Listener1 implements ApplicationListener<Event1>{

	public void onApplicationEvent(Event1 event) {
		String msg = event.getMsg();
		System.out.println("接收到信息"+msg);
	}

}
