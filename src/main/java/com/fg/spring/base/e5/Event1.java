package com.fg.spring.base.e5;

import org.springframework.context.ApplicationEvent;

public class Event1 extends ApplicationEvent{
	private static final long serialVersionUID = 1L;
	private String msg;
	public Event1(Object source,String msg) {
		super(source);
		this.msg = msg;
	}
	public String getMsg() {
		return msg;
	}
	public void setMsg(String msg) {
		this.msg = msg;
	}


}
