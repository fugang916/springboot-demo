package com.fg.spring.base.e5;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan("com.fg.spring.base.e5") 
public class Config {

}
