package com.fg.spring.base.e2;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class Domain {
	public static void main(String[] args) {
	    AnnotationConfigApplicationContext acac = new AnnotationConfigApplicationContext(Config.class);
	    Config c1 = acac.getBean(Config.class);
	    c1.m1();
	    acac.close();
	}
}
