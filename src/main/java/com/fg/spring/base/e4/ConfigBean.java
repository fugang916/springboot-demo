package com.fg.spring.base.e4;



public class ConfigBean {
	private String content;
	public ConfigBean(String content) {
		super();
		this.content = content;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
}
