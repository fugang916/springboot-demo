package com.fg.spring.base.e4;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;


public class Domain {
	public static void main(String[] args) {
		AnnotationConfigApplicationContext acac = new AnnotationConfigApplicationContext();
		acac.getEnvironment().setActiveProfiles("drv");
		acac.register(Config.class);
		acac.refresh();
		
		ConfigBean cb = acac.getBean(ConfigBean.class);
		System.out.println(cb.getContent());
		acac.close();
	}
}
