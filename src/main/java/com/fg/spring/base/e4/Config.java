package com.fg.spring.base.e4;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

@Configuration
public class Config {
	@Bean
	@Profile("prod")
	public ConfigBean prodConfig() {
		return new ConfigBean("prod");
	}
	
	@Bean
	@Profile("dev")	
	public ConfigBean devConfig() {
		return new ConfigBean("dev");
	}
}
