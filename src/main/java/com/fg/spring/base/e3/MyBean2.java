package com.fg.spring.base.e3;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

public class MyBean2 {
	@PostConstruct
	public void init() {
		System.out.println("init");
	}
	public MyBean2(){
		super();
		System.out.println("structure");
	}
	@PreDestroy
	public void destroy() {
		System.out.println("destroy");
	}
}
