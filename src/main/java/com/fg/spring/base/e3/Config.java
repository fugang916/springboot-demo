package com.fg.spring.base.e3;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan("com.fg.spring.base.e3")
public class Config {
	@Bean(initMethod="init",destroyMethod="destroy")
	MyBean1 myBean1() {
		return new MyBean1();
	}
	
	@Bean
	MyBean2 myBean2() {
		return new MyBean2();
	}
}
