package com.fg.spring.base.e3;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;


public class DoMain {

	public static void main(String[] args) {
		AnnotationConfigApplicationContext acac = new AnnotationConfigApplicationContext(Config.class);
		MyBean1 b1 = acac.getBean(MyBean1.class);
		MyBean2 b2 = acac.getBean(MyBean2.class);
		acac.close();
	}
}
