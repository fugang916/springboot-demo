package com.fg.spring.base.e1;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class DoMain {
	public static void main(String[] args) {
		AnnotationConfigApplicationContext acac = new AnnotationConfigApplicationContext(Config.class);
		Service1 s1 = (Service1) acac.getBean("service1");
		Service1 s2 = (Service1) acac.getBean("service1");
		Service2 s3 = (Service2) acac.getBean("service2");
		Service2 s4 = (Service2) acac.getBean("service2");
		System.out.println(s1.equals(s2));
		System.out.println(s3.equals(s4));
		acac.close();
	}
}
